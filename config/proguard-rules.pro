# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/damon/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:

-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}
#默认混淆配置
# This is a configuration file for ProGuard.
# http://proguard.sourceforge.net/index.html#manual/usage.html
#
# This file is no longer maintained and is not used by new (2.2+) versions of the
# Android plugin for Gradle. Instead, the Android plugin for Gradle generates the
# default rules at build time and stores them in the build directory.

#-dontusemixedcaseclassnames
#-dontskipnonpubliclibraryclasses
#-verbose

# Optimization is turned off by default. Dex does not like code run
# through the ProGuard optimize and preverify steps (and performs some
# of these optimizations on its own).

#-dontoptimize
#-dontpreverify

# Note that if you want to enable optimization, you cannot just
# include optimization flags in your own project configuration file;
# instead you will need to point to the
# "proguard-android-optimize.txt" file instead of this one from your
# project.properties file.

# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
    native <methods>;
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keepclassmembers class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator CREATOR;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn androidx.**

# Understand the @Keep support annotation.
-keep class androidx.annotation.Keep

-keep @androidx.annotation.Keep class * {*;}

-keepclasseswithmembers class * {
    @androidx.annotation.Keep <methods>;
}

-keepclasseswithmembers class * {
    @androidx.annotation.Keep <fields>;
}

-keepclasseswithmembers class * {
    @androidx.annotation.Keep <init>(...);
}

# 避免混淆Annotation、内部类、泛型、匿名类
-keepattributes *Annotation*,InnerClasses,Signature,EnclosingMethod

# 抛出异常时保留代码行号
-keepattributes SourceFile,LineNumberTable

# Owner about
-keep class com.example.medicalapp.been.** { *; }
-keepnames class com.example.medicalapp.ui.guide.SetAvatarFragment
-keepnames class com.example.medicalapp.ui.guide.SetGenderFragment
-keepnames class com.example.medicalapp.ui.guide.SetNicknameFragment

-keepnames class com.example.medicalapp.ui.dream.DreamHomeFragment
-keepnames class com.example.medicalapp.ui.dream.DreamListFragment
-keepnames class com.example.medicalapp.ui.dream.DreamPlanetFragment

-keepnames class com.example.medicalapp.ui.my.HistoryMessageListFragment
-keepnames class com.example.medicalapp.ui.my.MessageListFragment
-keepnames class com.example.medicalapp.ui.my.MyFragment
-keepnames class com.example.medicalapp.ui.my.MyPostFragment
-keepnames class com.example.medicalapp.ui.my.DeviceHelpFragment
-keepnames class com.example.medicalapp.ui.my.DreamHelpFragment


-keepnames class com.example.medicalapp.ui.sleep.start.Kang2sStartFragmnet
-keepnames class com.example.medicalapp.ui.sleep.record.Kang2sDayRecordFragment
-keepnames class com.example.medicalapp.ui.sleep.record.Kang2sMonthRecordFragment
-keepnames class com.example.medicalapp.ui.sleep.record.Kang2sWeekRecordFragment
-keepnames class com.example.medicalapp.ui.sleep.egg.WebSocketHelper


-keep class * implements androidx.viewbinding.ViewBinding {
    *;
}

#Base library: Rxhttp, RxLife...
-keep class com.app.base.been.** { *; }
-keep class com.app.base.net.**{*;}
-keep class com.rxjava.rxlife.**{*;}

#Eeg library
-keep class com.naolu.eeg.been.** { *; }
-keep class com.naolu.eeg.parse.** { *; }


#Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
# for DexGuard only
#-keepresourcexmlelements manifest/application/meta-data@value=GlideModule

#Wechat
-keep class com.tencent.mm.sdk.** {
   *;
}

#OkHttp3
# JSR 305 annotations are for embedding nullability information.
-dontwarn javax.annotation.**
# A resource is loaded with a relative path so the package of this class must be preserved.
-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase
# Animal Sniffer compileOnly dependency to ensure APIs are compatible with older versions of Java.
-dontwarn org.codehaus.mojo.animal_sniffer.*
# OkHttp platform used only on JVM and when Conscrypt dependency is available.
-dontwarn okhttp3.internal.platform.ConscryptPlatform

# Proguard configuration for Jackson 2.x
-keep class com.fasterxml.jackson.databind.ObjectMapper {
    public <methods>;
    protected <methods>;
}
-keep class com.fasterxml.jackson.databind.ObjectWriter {
    public ** writeValueAsString(**);
}
-keepnames class com.fasterxml.jackson.** { *; }
-dontwarn com.fasterxml.jackson.databind.**
# Proguard configuration for Jackson 2.x
-dontwarn com.fasterxml.jackson.databind.**
-keepclassmembers class * {
     @com.fasterxml.jackson.annotation.* *;
}

#http://stackoverflow.com/questions/18438890/menuitemcompat-getactionview-always-returns-null
#-keep class android.support.v7.widget.SearchView { *; }

#http://stackoverflow.com/questions/36879905/pre-lolipop-cannot-play-animatedvectordrawable-when-app-use-proguard
-keep class android.support.graphics.drawable.** { *; }

#EventBus
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }
# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}

-keep class com.tencent.mm.opensdk.** {
    *;
}

-keep class com.tencent.wxop.** {
    *;
}

-keep class com.tencent.mm.sdk.** {
    *;
}

#PictureSelector 2.0
-keep class com.luck.picture.lib.** { *; }
-dontwarn com.yalantis.ucrop**
-keep class com.yalantis.ucrop** { *; }
-keep interface com.yalantis.ucrop** { *; }
#Okio
-dontwarn org.codehaus.mojo.animal_sniffer.*

# 友盟SDK
-keep class com.umeng.** { *; }

-keep class com.uc.** { *; }

-keep class com.efs.** { *; }

-keepclassmembers class*{
     public<init>(org.json.JSONObject);
}
-keepclassmembers enum*{
      publicstatic**[] values();
      publicstatic** valueOf(java.lang.String);
}

#kotlin
-keep class kotlin.** { *; }
-keep class kotlin.Metadata { *; }
-dontwarn kotlin.**
-keepclassmembers class **$WhenMappings {
    <fields>;
}
-keepclassmembers class kotlin.Metadata {
    public <methods>;
}
-assumenosideeffects class kotlin.jvm.internal.Intrinsics {
    static void checkParameterIsNotNull(java.lang.Object, java.lang.String);
}
-keep class kotlinx.coroutines.android.** {*;}
# ServiceLoader support
-keepnames class kotlinx.coroutines.internal.MainDispatcherFactory {}
-keepnames class kotlinx.coroutines.CoroutineExceptionHandler {}
-keepnames class kotlinx.coroutines.android.AndroidExceptionPreHandler {}
-keepnames class kotlinx.coroutines.android.AndroidDispatcherFactory {}

# Most of volatile fields are updated with AFU and should not be mangled
-keepclassmembernames class kotlinx.** {
    volatile <fields>;
}